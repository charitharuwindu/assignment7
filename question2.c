#include <stdio.h>
#include <string.h>

int main()
{
    char str[100];
    char let;
    int i,frq=0;

    printf("Enter a sentence : \n");
    gets(str);

    printf("Enter the character to check frequency : ");
    scanf("%c",&let);

    for (i=0; i<strlen(str); i++)
    {
        if (str[i]==let)
            frq++;
    }
    printf("The frequency is : %d",frq);

    return 0;
}
