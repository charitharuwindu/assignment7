#include <stdio.h>
#define max 100

int main()
{
    int a[max][max],b[max][max],product[max][max], add[max][max];
    int arow, acolumn, brow, bcolumn;
    int i,j,k;
    int sum = 0;

    printf ("Enter the number of rows of first matrix :");
    scanf ("%d",&arow);

    printf ("\nEnter the number of columns of first matrix :");
    scanf ("%d",&acolumn);


    printf ("\nEnter the elements of first matrix :\n");
    for (i=0; i<arow; i++)
    {
        for (j=0; j<acolumn; j++)
        {
            scanf ("%d", &a[i][j]);
        }
    }

    printf ("\nEnter the number of rows of second matrix :");
    scanf ("%d",&brow);

    printf ("\nEnter the number of columns of second matrix  :");
    scanf ("%d",&bcolumn);

    if (brow != acolumn)
    {
        printf ("Matrix is not applicable");

    }
    else
    {
        printf ("Enter the elements of second matrix : \n");
        for (i=0; i<brow; i++)
        {
            for (j=0; j<bcolumn; j++)
            {
                scanf ("%d", &b[i][j]);
            }
        }

    }
    printf ("\n");
    for (i=0; i<arow; i++)
    {
        for (j=0; j<bcolumn; j++)
        {
            for (k=0; k<brow; k++)
            {
                sum += a[i][k] * b[k][j];
            }
            product[i][j]= sum;
            sum=0;
        }
    }
    printf ("\nMatrix multiplication\n");
    for (i=0; i<arow; i++)
    {
        for (j=0; j<bcolumn; j++)
        {
            printf ("%d ", product [i][j]);
        }
        printf("\n");
    }

    for (i=0; i<arow; i++)
    {
        for (j=0; j<bcolumn; j++)
        {
            add [i][j]=a[i][j] + b[i][j];
        }
    }
    printf ("\nMatrix addition\n");
    for (i=0; i<arow; i++)
    {
        for (j=0; j<bcolumn; j++)
        {
            printf ("%d ", add[i][j]);
        }
        printf("\n");
    }

    return 0;

}
