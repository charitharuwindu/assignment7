#include <stdio.h>
#define MAX 100

void reverse (char *str);
char str[MAX];

int main()
{
    printf("Enter a string to reverse : \n");
    scanf("%[^\n]s",str);

    printf("\nAfter reversing : ");

    reverse(str);

    return 0;
}

void reverse (char *str)
{
    if(*str)
    {
        reverse (str+1);
        printf ("%c",*str);
    }
}
